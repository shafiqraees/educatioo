
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Educatioo || Account verification</title>
    <style id="applicationStylesheet" type="text/css">
        .mediaViewInfo {
            --web-view-name: Google Pixel 4, 4XL – 1;
            --web-view-id: Google_Pixel_4_4XL__1;
            --web-scale-on-resize: true;
            --web-enable-deep-linking: true;
        }
        :root {
            --web-view-ids: Google_Pixel_4_4XL__1;
        }
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            border: none;
        }
        #Google_Pixel_4_4XL__1 {
            position: absolute;
            width: 412px;
            height: 870px;
            background-color: rgba(255,255,255,1);
            overflow: hidden;
            --web-view-name: Google Pixel 4, 4XL – 1;
            --web-view-id: Google_Pixel_4_4XL__1;
            --web-scale-on-resize: true;
            --web-enable-deep-linking: true;
        }
        #Image_1 {
            position: absolute;
            width: 206px;
            height: 74px;
            left: 29px;
            top: 76px;
            overflow: visible;
        }
        #Account_verification {
            left: 24px;
            top: 182px;
            position: absolute;
            overflow: visible;
            width: 173px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: bold;
            font-size: 19px;
            color: rgba(0,0,0,1);
        }
        #Dear_first_name_last_name {
            left: 24px;
            top: 220px;
            position: absolute;
            overflow: visible;
            width: 141px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #Welcome_to_the_Educatioo_Teach {
            left: 24px;
            top: 243px;
            position: absolute;
            overflow: visible;
            width: 345px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #To_enter_the_app_using_your_em {
            left: 24px;
            top: 296px;
            position: absolute;
            overflow: visible;
            width: 366px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #If_you_have_any_question_do_no {
            left: 24px;
            top: 335px;
            position: absolute;
            overflow: visible;
            width: 267px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #Kind_regards {
            left: 24px;
            top: 358px;
            position: absolute;
            overflow: visible;
            width: 65px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #The_Educatioo_Support_team {
            left: 24px;
            top: 381px;
            position: absolute;
            overflow: visible;
            width: 140px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #Should_your_link_not_work_plea {
            left: 24px;
            top: 532px;
            position: absolute;
            overflow: visible;
            width: 306px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(0,0,0,1);
        }
        #httpseducatioocomverifytrainer {
            left: 24px;
            top: 555px;
            position: absolute;
            overflow: visible;
            width: 307px;
            white-space: nowrap;
            text-align: left;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            color: rgba(9,213,255,1);
            text-decoration: underline;
        }
    </style>
</head>
<body>
<div id="Google_Pixel_4_4XL__1">
    <img id="Image_1" src="{{asset('public/assets/img/Image_1.png')}}">

    <div id="Account_verification">
        <span>Account verification</span>
    </div>
    <div id="Dear_first_name_last_name">
        <span>Dear {{ $details['body'] }},</span>
    </div>
    <div id="Welcome_to_the_Educatioo_Teach">
        <span>Welcome to the Educatioo Teaching and Assessment App. We sincerely<br/>hope you will find great joy in using our app with your students or<br/>employees. </span>
    </div>
    <div id="To_enter_the_app_using_your_em">
        <span>To enter the app using your email address and chosen password,</span><span style="color:rgba(0,177,243,1);"><a href="{{ isset($details['link']) ? $details['link'] : ""}}" target="_blank"> click here.</a></span>
    </div>
    <div id="If_you_have_any_question_do_no">
        <span>If you have any question, do not hesitate to contact us.</span>
    </div>
    <div id="Kind_regards">
        <span>Kind regards,</span>
    </div>
    <div id="The_Educatioo_Support_team">
        <span>The Educatioo Support team</span>
    </div>
    <div id="Should_your_link_not_work_plea">
        <span>Should your link not work, please copy this url in your browser.</span>
    </div>
    <div id="httpseducatioocomverifytrainer">
        <span>{{ isset($details['link']) ? $details['link'] : ""}}</span>
    </div>
</div>
</body>
</html>
