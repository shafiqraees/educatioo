<!DOCTYPE html>
<html>
    <head>
        <title>Educatioo</title>
    </head>

    <body>
        <img id="Image_1" src="{{asset('public/assets/img/Image_1.png')}}">
        <h1 style="padding-top: 20px;">{{ $details['title'] }}</h1>
        <p>{{ $details['body'] }}</p>
        @if(isset($details['trainee_group']) && $details['trainee_group'] != null)
            <p>Trainee Group: {{ isset($details['trainee_group']) ? $details['trainee_group'] : "" }}</p>
        @endif
        @if(isset($details['Course_Code']) && $details['Course_Code'] != null)
            <p>Course Code: {{ isset($details['Course_Code']) ? $details['Course_Code'] : "" }}</p>
        @endif
        <p>Trainee Key: {{ isset($details['pincode']) ? $details['pincode'] : "" }}</p>
        <p>We wish you lots of success using our Educatioo teaching and assessment app. </p>
        <p>Kind regards, </p>
        <p>Support team Educatioo</p>
    </body>

</html>
