<footer class="footer p-0">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="{{ url('/') }}" class="nav-link">Educatioo</a>
                </li>
                <li>
                    <a href="{{ url('/') }}" class="nav-link">About Us</a>
                </li>
                <li>
                    <a href="{{ url('/') }}" class="nav-link">Contact Us</a>
                </li>
            </ul>
        </nav>
    </div>
</footer>
