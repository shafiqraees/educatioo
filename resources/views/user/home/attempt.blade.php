@extends('user.layouts.main')
@section('content')
    <link rel="stylesheet" href="{{ asset('public/assets/css/fancybox.min.css') }}"/>
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-8">
                @if (session()->has('success'))
                    <div class="alert alert-success"> @if(is_array(session('success')))
                            <ul>
                                @foreach (session('success') as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        @else
                            {{ session('success') }}
                        @endif </div>
                @endif
                @if ($errors->any())
                    <a class="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @else
                    <form id="LoginValidation" action="{{route('quiz.save')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card ">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <h4 class="card-title">Quiz form</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    @php
                                        $x = 'A';
                                    @endphp
                                    @if(!empty($data))
                                        @if(isset($data))
                                            <h4 class="pl-3">{{$data->serial_id}}. {{$data->name}}?</h4>
                                            <br>
                                            @if(!empty($data->image) && ($data->image != 'default.png'))
                                                <div class="ml-auto mr-5">

                                                    <a href="{{Storage::disk('public')->exists('lg/'.$data->image) ? Storage::disk('public')->url('lg/'.$data->image) : Storage::disk('public')->url('default.png')}}" class="data-fancybox-mob"
                                                       data-fancybox="gallery">
                                                        <img src="{{Storage::disk('public')->exists('md/'.$data->image) ? Storage::disk('public')->url('md/'.$data->image) : Storage::disk('public')->url('default.png')}}"
                                                             class="pull-right rounded" alt="option" style="height: 100px;width: 100px;">
                                                    </a>
                                                </div>
                                            @endif
                                            @if(isset($data->option))
                                                @foreach($data->option as $key => $option)
                                                    <div class="col-sm-12 checkbox-radios">
                                                        <div class="form-check">
                                                            <label class="form-check-label">{{$x}} </label> <label class="form-check-label">
                                                                <input class="form-check-input selectedbutton" type="radio" name="question_option_id" value="{{$option->id}}" hidden required> {{$option->name}}
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                            <input type="hidden" name="user_quiz_id" value="{{$UserQuiz->id}}">
                                                            <input type="hidden" name="launch_quiz_id" value="{{$UserQuiz->launch_quiz_id}}">
                                                            <input type="hidden" name="suggested_question_id" value="{{$UserQuiz->suggested_question_id}}">
                                                            <input type="hidden" name="question_id" value="{{$data->id}}">
                                                            <input type="hidden" name="question_paper_id" value="{{$UserQuiz->question_paper_id}}">

                                                            @if(!empty($option->image) && ($option->image != 'default.png'))
                                                                <a href="{{Storage::disk('public')->exists('lg/'.$option->image) ? Storage::disk('public')->url('lg/'.$option->image) : Storage::disk('public')->url('default.png')}}"
                                                                   class="data-fancybox-mob" data-fancybox="gallery">
                                                                    <img src="{{Storage::disk('public')->exists('md/'.$option->image) ? Storage::disk('public')->url('md/'.$option->image) : Storage::disk('public')->url('default.png')}}"
                                                                         class="ml-5 rounded" alt="option" style="height: 60px;width: 60px;">
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @php   $x++; @endphp
                                                @endforeach
                                                {{-- <div class="col-sm-12 checkbox-radios">
                                                     <div class="form-check">
                                                         <label class="form-check-label">
                                                             <input class="form-check-input selectedbutton" type="checkbox" name="status" value="Completed"> Do you want to add in result?
                                                             <span class="circle">
                                                             <span class="check"></span>
                                                             </span>
                                                         </label>
                                                     </div>
                                                 </div>--}}
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" id="quizanswer" name="quizanswer" value="{{isset($answer_data->name) ? $answer_data->name : "" }}">
                            <input type="hidden" id="question_data" name="Question" value="{{isset($question_data->name) ? $question_data->name : "" }}">
                            <input type="hidden" id="option_data" name="option_data" value="{{isset($option_data->name) ? $option_data->name : "" }}">
                            <input type="hidden" id="feefback" name="feefback" value="{{isset($option_data->Feedback) ? $option_data->Feedback : "" }}">
                            <input type="hidden" id="attempt_data" name="attempt_data" value="{{isset($attempt->question_count) ? $attempt->question_count : "" }}">
                            <input type="hidden" id="result" name="result" value="{{isset($result) ? $result : "" }}">
                            <input type="hidden" id="total_question_points" name="total_question_points" value="{{isset($total_question_points) ? $total_question_points : "" }}">
                            <input type="hidden" id="attempted_option_points" name="attempted_option_points" value="{{isset($attempted_option_points) ? $attempted_option_points : "" }}">
                            <input type="hidden" id="attempted_option" name="attempted_option" value="{{isset($attempted_option) ? $attempted_option : "" }}">
                            <input type="hidden" id="logout_url" name="logout_url" value="{{route('logout.user') }}">
                            <div class="card-footer ml-auto  col-xs-offset-2 pull-right">
                                <button type="button" class="btn btn-rose nextbutton">Next</button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
<script src="{{asset('public/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/fancybox.min.js') }}"></script>
<script>

    $(document).ready(function () {
        $(document).on('click', '.nextbutton', function () {
            let val = $('input[name="question_option_id"]:checked').val();
            if (val != null && val != '') {
                $(this).closest('form').submit();
            } else {
                toastr.warning('Please Select an Option');
            }
        });
    });

    /*
    html: '<select class="form-control select2" aria-invalid="false" name="class_room" required>'+"\n" +
                    '<option value="">Select Class Room</option>'+"\n" +
        '{{--@foreach($data as $room)--}}'+"\n" +
        '<option value="{{--{{$room->id}}">{{$room->name}}--}}</option>'+"\n" +
        '{{--@endforeach--}}'+"\n" +
    '</select>',

    $(document).on('click','.nextbutton',function(e){
        e.preventDefault();
        var pin_code = $('#pin_code').val();
        var check = $('.selectedbutton:checked').val();
        alert(check);
        return false;
        var url = $('#url').val();
        var trainee_login  = $('#traineelogin').val();
        var _token =  $('meta[name="csrf-token"]').attr('content');
        if (pin_code){
            ajaxOnChangeRequest(pin_code,url,_token,trainee_login);
        } else {
            toastr.warning('Please enter trainee pin!')
        }

    });
    function ajaxOnChangeRequest(pin_code,url,_token,trainee_login) {
        var str = '';
        $.ajax({
            url:url,
            type:"post",
            data:{
                pin_code:pin_code,
                _token: _token
            },
            success:function(response){
                console.log(response);
                traineeLogin(response,trainee_login,_token);
            },
            error     : function (result){
                toastr.error('Sorry Record not foud!')
            }
        });
    }
    function traineeLogin(response,url,_token) {
        var home  = $('#home').val();
        swal({
            title: "Are you"+ response.data['name']+"?",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-dark",
                    closeModal: false,
                },
                confirm: {
                    text: "Ok",
                    value: true,
                    visible: true,
                    className: "btn-dark",
                    closeModal: false
                }
            }
        }).then(isConfirm => {

            if (isConfirm) {

                $.ajaxSetup({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                $.ajax({
                    type:'post',
                    url:url,
                    data:{id:response.data['id']},
                    success: function (results) {
                        console.log(results);
                        if (results.data) {
                            toastr.success('Login Successfully!')
                            window.location = home;
                        } else {
                            swal("Error!", results.message, "error");
                        }
                    }
                });

            } else {
                swal("Cancelled","Your profile is safe.");
            }
        });
    }*/

    $(document).ready(function () {
        var question_data = $('#question_data').val();
        var quizanswer = $('#quizanswer').val();
        var option_data = $('#option_data').val();
        var attempt_data = $('#attempt_data').val();
        var result = parseInt($('#result').val());
        var total_question_points = $('#total_question_points').val();
        var attempted_option_points = $('#attempted_option_points').val();
        var attempted_option = $('#attempted_option').val();
        let result_ = parseInt((parseFloat(attempted_option_points) / parseFloat(total_question_points)) * 100)
        var logout_url = $('#logout_url').val();
        var status = "Incorrect";
        var statusText = "Ok";
        var feedback = $('#feefback').val();
        if (option_data === quizanswer) {
            status = "Correct"
            statusText = "Cancel"
        }
        /*if (question_data) {
            swal({
                title: status,
                text: feedback,
                icon: "success",
                showCancelButton: true,
                buttons: {
                    confirm: {
                        text: statusText,
                        value: true,
                        visible: true,
                        className: "btn-dark",
                        closeModal: true
                    }
                }
            })
        }*/
        if (result) {
            swal({
                title: 'Finished',
                html: true,
                // text: "Score:" + "\n" + attempted_option + "/" + attempt_data + "\n" + "Percent: " + "\n" + result + "% \r\n you scored " + attempted_option_points + " out of a possible " + total_question_points + " point.",
                text: 'You Scored ' + attempted_option_points + ' out of ' + total_question_points + ' points. You Scored ' + result_ + '%',
                icon: "success",
                showCancelButton: true,
                buttons: {
                    confirm: {
                        text: "Ok",
                        value: true,
                        visible: true,
                        className: "btn-dark",
                        closeModal: true
                    }
                },
                content: '123'
            }).then(isConfirm => {
                if (isConfirm) {

                    $.ajaxSetup({

                        headers: {

                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                        }

                    });

                    $.ajax({
                        type: 'post',
                        url: logout_url,
                        success: function (results) {
                            location.reload();
                        }
                    });

                } else {
                    location.reload();
                }
            });
        }

    });
</script>
