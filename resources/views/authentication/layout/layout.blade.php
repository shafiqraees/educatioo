<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('authentication.layout.head')
<body class="off-canvas-sidebar">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
        <div class="navbar-wrapper text-sm-left text-xs-center">
            <a class="" href="{{url('/')}}">
                <img src="{{asset('public/assets/img/Educatioo.png')}}" style="max-width: 90px">
            </a>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url({{Storage::disk('public')->exists('bg_2.webp') ? Storage::disk('public')->url('bg_2.webp') : Storage::disk('public')->url('default.png')}}); background-size: cover; background-position: top center;">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="container">
            @section('content')
        </div>
    </div>
</div>
<!--   Core JS Files   -->

@show
@include('authentication.layout.footer_script')
</body>
</html>
