@extends('admin.layouts.main')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @if (session()->has('success'))
                <div class="alert alert-success"> @if(is_array(session('success')))
                        <ul>
                            @foreach (session('success') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @else
                        {{ session('success') }}
                    @endif </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">

                <div class="col-md-6">
                    <form id="RegisterValidation" action="{{route('update.package')}}" method="post">
                        @csrf
                        <div class="card ">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">mail_outline</i>
                                </div>
                                <h4 class="card-title">Basic Package</h4>
                            </div>
                            <div class="card-body ">
                                <div class="form-group">
                                    <label for="exampleEmail" class="bmd-label-floating"> Number of Trainer *</label>
                                    <input type="number" value="{{isset($basic->number_of_trainer) ? $basic->number_of_trainer : ""}}" class="form-control" name="number_of_trainer" id="exampleEmail" required="true">
                                    <input type="hidden" value="{{isset($basic->id) ? $basic->id : ""}}" name="id">
                                </div>
                                <div class="form-group">
                                    <label for="examplePassword" class="bmd-label-floating"> Number of Trainee Groups *</label>
                                    <input type="number" class="form-control" name="number_of_trainee_groups" value="{{isset($basic->number_of_trainee_groups) ? $basic->number_of_trainee_groups : ""}}" id="examplePassword" required="true">
                                </div>
                                <div class="form-group">
                                    <label for="examplePassword1" class="bmd-label-floating"> Number of Courses *</label>
                                    <input type="number" class="form-control" id="examplePassword1" required="true" name="number_of_courses" value="{{isset($basic->number_of_courses) ? $basic->number_of_courses : ""}}">
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-rose">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="LoginValidation" action="{{route('update.package')}}" method="post">
                        @csrf
                        <div class="card ">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">contacts</i>
                                </div>
                                <h4 class="card-title">Premium Package</h4>
                            </div>
                            <div class="card-body ">
                                <div class="form-group">
                                    <label for="exampleEmail" class="bmd-label-floating"> Number of Trainer *</label>
                                    <input type="number" class="form-control" name="number_of_trainer" value="{{isset($premium->number_of_courses) ? $premium->number_of_courses : ""}}" id="exampleEmail" required="true">
                                </div>
                                <div class="form-group">
                                    <label for="examplePassword" class="bmd-label-floating"> Number of Trainee Groups *</label>
                                    <input type="number" class="form-control" name="number_of_trainee_groups" value="{{isset($premium->number_of_trainee_groups) ? $premium->number_of_trainee_groups : ""}}" id="examplePassword" required="true">
                                </div>
                                <div class="form-group">
                                    <label for="examplePassword1" class="bmd-label-floating"> Number of Courses *</label>
                                    <input type="number" class="form-control" id="examplePassword1" required="true" name="number_of_courses" value="{{isset($premium->number_of_courses) ? $premium->number_of_courses : ""}}">
                                </div>
                                <input type="hidden" value="{{isset($premium->id) ? $premium->id : ""}}" name="id">
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-rose">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
@endsection
