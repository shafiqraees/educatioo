@extends('teacher.layouts.main')
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                @if (session()->has('success'))
                <div class="alert alert-success"> @if(is_array(session('success')))
                    <ul>
                        @foreach (session('success') as $message)
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                    @else
                    {{ session('success') }}
                    @endif
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {{--<div class="card card-stats">--}}
                <div class="ml-auto mr-auto text-center">
                    <img src="{{asset('public/forntend/images/trainer-educatioo.png')}}" width="40%">

                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>
@endsection