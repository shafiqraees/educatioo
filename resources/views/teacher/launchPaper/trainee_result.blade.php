@extends('teacher.layouts.main')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">launch</i>
                            </div>
                            <h4 class="card-title">Attempted Students</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-3 col-md-6">
                                    <div class="form-actions">
                                        <a href="{{ route('trainee.resultTable',[$id, $classroom]) }}" class="btn btn-primary">
                                            <span class="la la-plus font-medium-3"></span>
                                            Result Detailed
                                            <div class="ripple-container"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            @if (session()->has('success'))
                                <div class="alert alert-success"> @if(is_array(session('success')))
                                        <ul>
                                            @foreach (session('success') as $message)
                                                <li>{{ $message }}</li>
                                            @endforeach
                                        </ul>
                                    @else
                                        {{ session('success') }}
                                    @endif </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Trainee Name</th>
                                            <th>Trainee email</th>
                                            <th>Trainee Result Score</th>
                                            <th>Trainee Result Percentage</th>
                                            <th>Created Date</th>
                                        </tr>
                                    </thead>
                                    {{--<tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Trainee Name</th>
                                        <th>Trainee email</th>
                                        <th>Trainee Result Score</th>
                                        <th>Trainee Result Percentage</th>
                                        <th>Created Date</th>
                                    </tr>
                                    </tfoot>--}}
                                    <tbody>
                                        @if(!empty($data))
                                            @foreach($data as $row)
                                                <tr>
                                                    @php
                                                        $marks = 0;
                                                        $total_marks = 0;
                                                    @endphp

                                                    @if(isset($row->userAttemptQuiz[0]))
                                                    @foreach($row->userAttemptQuiz[0]->launchQuiz->questionPaper->question as $question )
                                                        @if($question->include_in_result == true)
                                                            @php
                                                                $total_marks += $question->marks;
                                                            @endphp
                                                            @foreach($row->userAttemptQuiz as $attempt)
                                                                @if($question->id == $attempt->question->id)
                                                                    @php
                                                                        $option = $attempt->answerOption->answer;
                                                                        if (($option != "") || ($option != null)){
                                                                            $marks += $question->marks;
                                                                        }
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                    @endif
                                                    @php
                                                        $score = $marks . '/' . $total_marks;
                                                        $marks_percentage = ($marks / (($total_marks > 0) ? $total_marks : 1)) * 100;
                                                    @endphp
                                                    <td>{{$row->id}} </td>
                                                    <td>{{isset($row->name) ? $row->name : ""}} </td>
                                                    <td>{{isset($row->email) ? $row->email : ""}} </td>
                                                    <td>{{isset($score) ? $score : ""}} </td>
                                                    <td>{{isset($marks_percentage) ? number_format($marks_percentage) ."%" : ""}}</td>
                                                    <td>{{!empty($row->created_at->diffForHumans()) ? $row->created_at->diffForHumans() : ""}}</td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
@endsection
