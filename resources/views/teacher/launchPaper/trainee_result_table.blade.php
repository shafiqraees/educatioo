@extends('teacher.layouts.main')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-icon card-header-primary">
                                <div class="card-icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title ">Attempted Students</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-3 col-md-6">
                                        <div class="form-actions">
                                            <a href="{{ route('trainee.result',[$id, $classroom]) }}" class="btn btn-primary">
                                                <span class="la la-plus font-medium-3"></span>
                                                Result
                                                <div class="ripple-container"></div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>

                                                <th class="font-weight-bold text-dark">

                                                </th>
                                                <th class="font-weight-bold text-dark text-center">
                                                    Question Number
                                                </th>
                                                @if(!empty($data))
                                                    @php $count = 0; @endphp
                                                    @foreach($data as $row)
                                                        @if(isset($row->userAttemptQuiz[0]))
                                                            @foreach($row->userAttemptQuiz[0]->launchQuiz->questionPaper->question as $question )
                                                                @if($question->include_in_result == true)
                                                                    @php $count++; @endphp
                                                                    <th class="text-center font-weight-bold text-dark">
                                                                        {{ $question->serial_id }}
                                                                    </th>
                                                                @endif
                                                            @endforeach
                                                        @break
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <th class="font-weight-bold text-dark text-right">

                                                </th>
                                                <th class="font-weight-bold text-dark text-right">

                                                </th>
                                            </tr>
                                            <tr>

                                                <th class="font-weight-bold text-dark">

                                                </th>
                                                <th class="font-weight-bold text-dark text-center">
                                                    Points
                                                </th>
                                                @if(!empty($data))
                                                    @foreach($data as $row)

                                                        @if(isset($row->userAttemptQuiz[0]))
                                                        @foreach($row->userAttemptQuiz[0]->launchQuiz->questionPaper->question as $question )
                                                            @if($question->include_in_result == true)
                                                                <th class="text-center font-weight-bold text-dark">
                                                                    {{ $question->marks }}
                                                                </th>
                                                            @endif
                                                        @endforeach
                                                        @break
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <th class="font-weight-bold text-dark text-right">

                                                </th>
                                                <th class="font-weight-bold text-dark text-right">

                                                </th>
                                            </tr>
                                            <tr>

                                                <th class="font-weight-bold text-dark">
                                                    Id
                                                </th>
                                                <th class="font-weight-bold text-dark text-center">
                                                    Trainee Name
                                                </th>
                                                @if(!empty($data))
                                                    @foreach($data as $row)
                                                        @if(isset($row->userAttemptQuiz[0]))
                                                        @foreach($row->userAttemptQuiz[0]->launchQuiz->questionPaper->question as $question )
                                                            @if($question->include_in_result == true)
                                                                <th class="text-center font-weight-bold text-dark"></th>
                                                            @endif
                                                        @endforeach
                                                        @break
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <th class="font-weight-bold text-dark text-right">
                                                    Score
                                                </th>
                                                <th class="font-weight-bold text-dark text-right">
                                                    Result
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if(!empty($data))
                                                @foreach($data as $row)
                                                    @php
                                                        $result = 0;
                                                        $marks = 0;
                                                        $score = 0;
                                                        $marks_percentage = 0;
                                                        $total_marks = 0;
                                                    @endphp
                                                    <tr>
                                                        <td style="background: #f5f7f8">
                                                            {{$row->id}}
                                                        </td>
                                                        <td class="text-center" style="background: #f5f7f8">
                                                            {{isset($row->name) ? $row->name : ""}}
                                                        </td>
                                                        @if(isset($row->userAttemptQuiz[0]))
                                                        @foreach($row->userAttemptQuiz[0]->launchQuiz->questionPaper->question as $question )
                                                            @if($question->include_in_result == true)
                                                                @php
                                                                    $total_marks += $question->marks;
                                                                @endphp
                                                                <td>
                                                                    @foreach($row->userAttemptQuiz as $attempt)
                                                                        @if($question->id == $attempt->question->id)
                                                                            @php
                                                                                $option = $attempt->answerOption->answer;
                                                                                if (($option != "") || ($option != null)) {
                                                                                   $result ++;
                                                                                   $marks += $attempt->question->marks;
                                                                                }
                                                                                $score =  $marks. '/'.$total_marks;
                                                                                $marks_percentage =  $marks/(($total_marks != 0 ) ? $total_marks : 1)*100;
                                                                            @endphp
                                                                            @php
                                                                                $option = $attempt->answerOption->answer;
                                                                            @endphp
                                                                            @if (($option != "") || ($option != null))
                                                                                <p class="text-center mb-0" style="background: #a6dcb1; padding: 3px 20px;">
                                                                                    <i class="fa fa-check"></i>
                                                                                </p>
                                                                            @else
                                                                                <p class="text-center mb-0" style="background: #f39999; padding: 3px 20px;">
                                                                                    <i class="fa fa-times"></i>
                                                                                </p>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                        @else
                                                                @for($i=0; $i<$count; $i++)
                                                            <td>
                                                            </td>
                                                            @endfor
                                                        @endif

                                                        <td class="text-right">
                                                            {{isset($score) ? $score : ""}}
                                                        </td>
                                                        <td class="text-right">
                                                            {{isset($marks_percentage) ? number_format($marks_percentage) ."%" : ""}}
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $result = 0;
                                                        $count_attemt = 0;
                                                    @endphp
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
