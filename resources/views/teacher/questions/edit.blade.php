@extends('teacher.layouts.main')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('public/assets/css/fancybox.min.css') }}"/>
    <div class="container-fluid">
        @if (session()->has('success'))
            <div class="alert alert-success"> @if(is_array(session('success')))
                    <ul>
                        @foreach (session('success') as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                @else
                    {{ session('success') }}
                @endif
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <form id="LoginValidation" action="{{route('question.update',$data->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">contacts</i>
                            </div>
                            <h4 class="card-title">Course Name: {{isset($course->name) ? $course->name : ""}}</h4>
                        </div>
                        <div class="card-body ">
                            <div class="row mb-3">
                                <div class="col-md-8" id="option_dorm">
                                    <div class="row mb-3 mb-sm-0">
                                        <div class="col-1 p-0">
                                            <div class="form-group text-center">
                                                <!-- <label for="">Qs #</label> -->
                                                <input type="text" class="form-control font-weight-bold align-items-center text-center bg-transparent " id="question_number" value="{{$data->serial_id}}" name="question_number" readonly
                                                       style="text-align: center">
                                            </div>
                                        </div>
                                        <div class="col-11">
                                            <div class="form-group">
                                                <textarea name="name" class="form-control" id="name" placeholder="Enter Question here" rows="1" required oninput="myFunction()">{{$data->name}}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row align-items-center p-0">
                                        @if(isset($data->option))
                                            @foreach($data->option as $key => $option)
                                                <div class="col-12 col-md-10 d-flex align-items-center">
                                                    <div class="form-check text-left ">
                                                        <input class="form-check-input custom_checkbox" type="radio" name="answer" id="blankRadio1" value="{{$key}}" {{ ($option->answer == $key)? "checked" : "" }}aria-label="...">
                                                    </div>
                                                    <div class=" rounded bg-light border-light px-1 w-100 ml-2">
                                                        <div class="input-group w-100 align-items-center">
                                                            <textarea name="option[]" class="form-control" id="name" placeholder="Enter Option here" rows="1" required oninput="myFunction()"> {{$option->name}}</textarea>
                                                            <input type="hidden" class="form-control" id="name" name="option_id[]" value="{{$option->id}}">
                                                            <div class="input-group-prepend align-items-center">
                                                                <div class="image-upload mt-3 fileinput  fileinput-sm fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview fileinput-preview fileinput-exists thumbnail"></div>
                                                                    <div class="d-flex align-items-center">
                                                                        <label for="file-input">
                                                                            <span class="input-group-text fileinput-new">
                                                                                <i class="material-icons text-secondary" style="font-size:16px">add</i> <i class="material-icons text-secondary" style="font-size:16px">image</i>
                                                                            </span>

                                                                        </label>
                                                                        <div class="mt-n5">
                                                                            <span class="btn btn-rose btn-round btn-sm btn-file p-1">
                                                                                <span class="fileinput-new p-0" style="font-size:8px">Select image</span>
                                                                                <span class="fileinput-exists p-0" style="font-size:8px">Change</span>
                                                                                <input type="file" class="image_file" name="image[]" accept="image/x-png,image/gif,image/jpeg"/>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-md-1 p-sm-0 d-flex align-items-center">
                                                    <div class="form-group ">
                                                        <input type="number" placeholder="Go to Qs" class="form-control" id="name" name="question_id[]" value="{{$option->suggested_question_id}}">
                                                    </div>
                                                </div>
                                                <div class="col-4 col-md-1 d-flex align-items-center">
                                                    <span class="material-icons ml-4 bg-transparent">
                                                        clear
                                                    </span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-4 mb-3 mt-3 mb-sm-0 mt-sm-0">
                                    <div class="fileinput fileinput-new text-center " data-provides="fileinput" style="display: block !important;">
                                        <div class="fileinput-new thumbnail" style="box-shadow: none !important;">
                                            <a href="{{Storage::disk('public')->exists('lg/'.$data->image) ? Storage::disk('public')->url('lg/'.$data->image) : asset('public/assets/img/placeholder.jpg')}}" class="data-fancybox-mob"
                                               data-fancybox="gallery">
                                                <img src="{{Storage::disk('public')->exists('md/'.$data->image) ? Storage::disk('public')->url('md/'.$data->image) : asset('public/assets/img/placeholder.jpg')}}" alt="..." style="width: 120px; height: 120px;">
                                            </a>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 120px; height: 120px;">

                                        </div>
                                        <div>
                                            <span class="btn btn-rose btn-round btn-sm btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" class="image_file" name="photo" accept="image/x-png,image/gif,image/jpeg"/>
                                            </span>
                                            <a href="#pablo" class="btn btn-danger btn-sm btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <!-- <label for="">Points *</label> -->
                                                <input type="text" class="form-control" placeholder="Pts" id="points" required="true" value="{{$data->marks}}" name="points">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="type" id="status" class="form-control" required="true">
                                                    <option value="Multiple Choice">MC</option>
                                                </select>
                                                {{--<label for="">Multiple Choice</label>
                                                    <input type="text" name="" class="form-control" id="">--}}
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-4 mt-sm-0">
                                            <div class="form-group ">
                                                <input type="checkbox" name="include_in_result" class="form-check-input" value="true" id="exampleCheck1"
                                                    {{ ($data->include_in_result == "true")? "checked" : "" }}>
                                                <label class="form-check-label" for="exampleCheck1">Include In Result</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-left ">
                                            <div class="form-group ">
                                                <input type="checkbox" name="final_question" class="form-check-input" value="true" id="exampleCheck1"
                                                    {{ ($data->final_question == "true")? "checked" : "" }}>
                                                <label class="form-check-label" for="exampleCheck1">Final Question</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <input type="hidden" value="{{isset($data->paper->name) ? $data->paper->id : ""}}" name="id"/>
                            <input type="button" value="Add more options" class="add btn btn-rose" id="addmore"/>
                            <button type="submit" class="btn btn-rose">Submit</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">

                        <h4 class="card-title">All Question of <strong>{{isset($course->name) ? $course->name : ""}}</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        @if (session()->has('success'))
                            <div class="alert alert-success"> @if(is_array(session('success')))
                                    <ul>
                                        @foreach (session('success') as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    {{ session('success') }}
                                @endif
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Qs ID</th>
                                        <th>Question</th>
                                        <th>Go To</th>
                                        <th>Image</th>
                                        <th class="text-center">Actions</th>
                                        <th class="text-center">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($allQuestions))
                                        @foreach($allQuestions as $row)
                                            <tr>
                                                <td>{{$row->serial_id}} </td>

                                                <td>
                                                    {{$row->name}} <br> @foreach($row->option as $option)
                                                        <div class="radio d-inline">
                                                            <label class="">
                                                                <input type="radio" name="{{$option->name}}{{$row->id}}" {{ ($option->answer != "") ? "checked" : "" }} disabled>
                                                                {{$option->name}}</label>
                                                            @if(Storage::disk('public')->exists('xs/'.$option->image))
                                                                <a href="{{Storage::disk('public')->exists('lg/'.$option->image) ? Storage::disk('public')->url('lg/'.$option->image) : Storage::disk('public')->url('default.png')}}"
                                                                   class="data-fancybox-mob" data-fancybox="gallery">
                                                                    <img src="{{Storage::disk('public')->exists('xs/'.$option->image) ? Storage::disk('public')->url('xs/'.$option->image) : Storage::disk('public')->url('default.png')}}"
                                                                         class=" height-150" alt="Card image" height="50px">
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($row->option as $option)
                                                        <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="{{$option->name}}">
                                                            {{ ($option->suggested_question_id != "")? $option->suggested_question_id : "" }}
                                                        </button>
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @if(Storage::disk('public')->exists('xs/'.$row->image))
                                                        <div class="d-flex flex-column">
                                                            <a href="{{Storage::disk('public')->exists('lg/'.$row->image) ? Storage::disk('public')->url('lg/'.$row->image) : Storage::disk('public')->url('default.png')}}"
                                                               class="data-fancybox-mob" data-fancybox="gallery">
                                                                <img src="{{Storage::disk('public')->exists('xs/'.$row->image) ? Storage::disk('public')->url('xs/'.$row->image) : Storage::disk('public')->url('default.png')}}" width="60px"
                                                                     class="mt-n5">
                                                            </a>
                                                        </div>
                                                    @endif

                                                </td>

                                                <td class="text-right">
                                                    {{--<a href="javascript:void(0)" class="text-info like editpopup"
                                                                data-action="{{route('question.edit',$row->id)}}"><i
                                                        class="material-icons">edit</i></a>--}}
                                                    <a href="{{route('question.edit',$row->id)}}" class="text-info"><i class="material-icons">edit</i></a>
                                                    <a href="javascript:void(0)" class="text-danger remove" data-url="{{route('question.destroy',$row->id)}}"><i class="material-icons">close</i></a>
                                                </td>

                                                <td class="text-center">

                                                    <div class="form-group " data-toggle="tooltip" data-placement="right" title="Final Question">
                                                        <input type="checkbox" name="{{$row->final_question}}" class="form-check-input" value="true" id="exampleCheck1" {{ ($row->final_question != "") ? "checked" : "" }} disabled>
                                                    </div>
                                                    <div class="form-group ">
                                                        <input type="checkbox" {{ ($row->include_in_result != "") ? "checked" : "" }}name="include_in_result" class="form-check-input" value="true" id="exampleCheck1" data-toggle="tooltip"
                                                               data-placement="bottom" title="Include in result">
                                                    </div>

                                                    <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Points">
                                                        {{ !empty($row->marks) ? $row->marks : "Not Assign" }}
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
    </div>
@endsection
<script src="{{asset('public/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/fancybox.min.js') }}"></script>
<script>
    $(window).on('load', function () {
        $("html, body").animate({
            scrollTop: $(document).height()
        }, 1000);
    });
    var count = 1;
    $(document).ready(function () {
        $("#addmore").click(function () {
            console.log('clicked')
            //var counter = parseInt($("#blankRadio1").val());
            var counter = parseInt($('.custom_checkbox').last().val());

            var lastField = $("#option_dorm div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<div class=\"row align-items-center p-0\" id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fName = '<div class="col-12 col-md-10 d-flex align-items-center">\n' +
                '<div class="form-check text-left">\n' +
                '<input class="form-check-input " type="radio" name="answer" value="' + (counter + 1) + '" id="blankRadio1" value="option1" aria-label="...">\n' +
                '</div>\n' +
                '<div class="mt-2 rounded bg-light border-light px-1 w-100 ml-2 bmd-form-group">\n' +
                '<div class="input-group w-100 align-items-center">\n' +
                '<textarea name="option[]"  id="name" class="form-control" placeholder="Enter Option here" rows="1" required="" oninput="myFunction()" aria-required="true"></textarea>\n' +
                '<div class="input-group-prepend align-items-center">\n' +
                '<div class="image-upload mt-3 fileinput  fileinput-sm fileinput-new" data-provides="fileinput">\n' +
                '<div class="fileinput-preview fileinput-preview fileinput-exists thumbnail"></div>\n' +
                '<div class="d-flex align-items-center">\n' +
                '<label for="file-input">\n' +
                '<span class="input-group-text fileinput-new">\n' +
                '<i class="material-icons text-secondary" style="font-size:16px">add</i>\n' +
                '<i class="material-icons text-secondary" style="font-size:16px">image</i>\n' +
                '</span>\n' +
                '</label>\n' +
                '<div class="mt-n5">\n' +
                '<span class="btn btn-rose btn-round btn-sm btn-file p-1">\n' +
                '<span class="fileinput-new p-0" style="font-size:8px">Select image</span>\n' +
                '<span class="fileinput-exists p-0" style="font-size:8px">Change</span>\n' +
                '<input type="file" class="image_file" name="image[]" accept="image/x-png,image/gif,image/jpeg">\n' +
                '</span>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-8 col-md-1 p-sm-0 d-flex align-items-center">\n' +
                '<div class="form-group bmd-form-group">\n' +
                '<input type="number" placeholder="Go to Qs" class="form-control" id="name" name="question_id[]">\n' +
                '</div>\n' +
                '</div>';
            var removeButton = $(
                "<div class=\"col-4 col-md-1 d-flex align-items-center\"><span class=\"material-icons ml-4 bg-transparent\">\clear</span></div>"
            );

            removeButton.click(function () {
                $(this).parent().remove();
            });


            fieldWrapper.append(fName);

            fieldWrapper.append(removeButton);
            $("#option_dorm").append(fieldWrapper);
            $('.image_file').on('change', function () {
                if (this.files[0].size > 1048576) {
                    toastr.error('Image is too big');
                    this.value = "";
                }
            });
        });
        $(function () {
            $("[data-toggle='tooltip']").tooltip();
        });
    });
</script>

<script>
    // Dealing with Textarea Height
    function calcHeight(value) {
        let numberOfLineBreaks = (value.match(/\n/g) || []).length;
        // min-height + lines x line-height + padding + border
        let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
        return newHeight;
    }

    let textarea = document.querySelector(".resize-ta");
    textarea.addEventListener("keyup", () => {
        textarea.style.height = calcHeight(textarea.value) + "px";
    });
</script>

<script>
    function myFunction() {
        var textarea = document.getElementsByTagName("textarea");
        for (item of textarea) {
            var limit = 200;
            item.style.height = "";
            item.style.height = Math.min(item.scrollHeight, 300) + "px";
        }

    }
</script>

<script>
    $('img[data-enlargeable]').addClass('img-enlargeable').click(function () {
        var src = $(this).attr('src');
        var modal;

        function removeModal() {
            modal.remove();
            $('body').off('keyup.modal-close');
        }

        modal = $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: 'contain',
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0',
            left: '0',
            cursor: 'zoom-out'
        }).click(function () {
            removeModal();
        }).appendTo('body');
        //handling ESC
        $('body').on('keyup.modal-close', function (e) {
            if (e.key === 'Escape') {
                removeModal();
            }
        });
    });
</script>

@section('page-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
    <script>
        $('.image_file').change(function () {
            if (this.files[0].size > 1048576) {
                toastr.error('Image is too big');
                this.value = "";
            }
        });
    </script>
@endsection
