
<div class="row">
    <div class="col-md-12">
        <form id="LoginValidation"  action="{{route('question.update',$data->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">contacts</i>
                    </div>
                    {{--<h4 class="card-title">Course Name: {{isset($course->name) ? $course->name : ""}}</h4>--}}
                </div>
                <div class="card-body ">

                    <div class="row">
                        <div class="col-md-10" id="option_dorm">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="">Qs #</label>
                                        <input type="text" class="form-control" id="question_number"
                                               value="{{$data->serial_id}}" name="question_number" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Enter Question here</label>
                                        <input type="text" class="form-control" id="name" required="true"
                                               name="name" value="{{$data->name}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Multiple Choice</label>
                                        <input type="text" name="" class="form-control" id="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Points *</label>
                                        <input type="text" class="form-control" id="points" required="true"
                                               name="points" value="{{$data->marks}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <input type="checkbox" name="final_question" class="form-check-input" id="exampleCheck1"
                                               value="true" {{ ($data->final_question == "true")? "checked" : "" }}>
                                        <label class="form-check-label" for="exampleCheck1">Final Question</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <input type="checkbox" name="include_in_result" class="form-check-input" id="exampleCheck1"
                                               value="true" {{ ($data->include_in_result == "true")? "checked" : "" }}>
                                        <label class="form-check-label" for="exampleCheck1">Include In Result</label>
                                    </div>
                                </div>
                                @if(isset($data->option))
                                    @foreach($data->option as $key => $option)
                                <div class="col-md-2 d-flex align-items-center justify-content-center">
                                    <input class="form-check-input " type="radio" name="answer" id="blankRadio1"
                                           value="{{$key}}" {{ ($option->answer == $key)? "checked" : "" }} aria-label="...">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        <input type="text" class="form-control" id="name" name="option[]" value="{{$option->name}}">
                                        <input type="hidden" class="form-control" id="name" name="option_id[]" value="{{$option->id}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {{--<label for="">Go to Question ID
                                            *</label>--}}

                                        <input type="number" class="form-control" id="name" name="question_id[]" value="{{$option->suggested_question_id}}">
                                    </div>
                                </div>
                                <div class="col-md-3 text-right">
                                    <div class="fileinput fileinput-new d-flex align-items-center"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail1">
                                            <img src="{{Storage::disk('public')->exists('xs/'.$option->image) ? Storage::disk('public')->url('xs/'.$option->image) : Storage::disk('public')->url('default.png')}}"
                                                 width="50px">
                                        </div>
                                        <span class="btn-file">
                                                <span class="fileinput-new">Upload File</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image[]"
                                                       accept="image/x-png,image/gif,image/jpeg" />
                                            </span>
                                        <div class="fileinput-preview fileinput-exists thumbnail1"></div>
                                    </div>
                                </div>
                                <div class="col-md-2 d-flex align-items-center justify-content-start ">
                                    <a class="text-danger font-weight-bold " href="javascript:void(0)">X</a>
                                </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <div class="fileinput fileinput-new " data-provides="fileinput"
                                 style="display: block !important;">
                                <div class="fileinput-new thumbnail">
                                    {{--<img src="{{asset('public/assets/img/placeholder.jpg')}}" alt="...">--}}
                                    <img src="{{Storage::disk('public')->exists('md/'.$data->image) ? Storage::disk('public')->url('md/'.$data->image) : Storage::disk('public')->url('default.png')}}">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="photo" accept="image/x-png,image/gif,image/jpeg" />
                                        </span>
                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists"
                                       data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <input type="button" value="Add more options" class="add btn btn-rose" id="addmore" />
                    <input type="hidden" value="{{isset($data->paper->name) ? $data->paper->id : ""}}" name="id" />
                    <button type="submit" class="btn btn-rose">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{{asset('public/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script>
    $(window).on('load', function() {
        $("html, body").animate({
            scrollTop: $(document).height()
        }, 1000);
    });
    var count = 1;
    $(document).ready(function() {
        $("#addmore").click(function() {
            var lastField = $("#option_dorm div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<div class=\"row\" id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fName = '<div class="col-md-2 d-flex align-items-center justify-content-center">\n' +
                '<input class="form-check-input " type="radio" name="answer" value="' + (count++) +
                '" id="blankRadio1" value="option1" aria-label="...">\n' +
                '</div>\n' +
                '<div class="col-md-3">\n' +
                '<div class="form-group">\n' +
                '<label for="">Option</label>\n' +
                '<input type="text" class="form-control" id="name" name="option[]">\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-md-2">\n' +
                '<div class="form-group">\n' +
                '<label for="">Go to Question ID *</label>\n' +
                '<input type="number" class="form-control" id="name" name="question_id[]">\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-md-3 text-right">\n' +
                '<div class="fileinput fileinput-new d-flex align-items-center" data-provides="fileinput">\n' +
                '<div class="fileinput-new thumbnail1">\n' +
                '<img src="{{asset('public/assets/img/placeholder.jpg')}}" alt="..." width="10px">\n' +
                '</div>\n' +
                '<span class="btn-file">\n' +
                '<span class="fileinput-new">Upload File</span>\n' +
                '<span class="fileinput-exists">Change</span>\n' +
                '<input type="file" name="image[]" accept="image/x-png,image/gif,image/jpeg" />\n' +
                '</span>\n' +
                '<div class="fileinput-preview fileinput-exists thumbnail1">\n' +
                '</div>\n' +
                '</div>';

            // var removeButton = $("<a class=\"text-danger font-weight-bold\" href=\"javascript:void(0)\" />X</a>");
            var removeButton = $("<div class=\"col-md-2 d-flex align-items-center justify-content-start \"><a class=\"text-danger font-weight-bold \" href=\"javascript:void(0)\">X</a></div>");
            removeButton.click(function() {
                $(this).parent().remove();
            });
            fieldWrapper.append(fName);
            fieldWrapper.append(removeButton);
            $("#option_dorm").append(fieldWrapper);
        });

    });
</script>
