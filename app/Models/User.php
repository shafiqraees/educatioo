<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function className() {
        return $this->belongsTo(ClassRoom::class,'class_room_id');
    }
    public function classRoom() {
        return $this->belongsTo(ClassRoom::class,'class_room_id');
    }

    public function userAttemptQuiz() {
        return $this->hasMany(UserQuizAttempt::class);
    }
    public function userQuiz() {
        return $this->hasMany(UserQuiz::class);
    }

    public function userQuizSession() {
        return $this->hasManyThrough(Question::class,UserQuizAttempt::class)
            ->where('user_quiz_attempts',function ($query){
                $query->where('launch_quiz_id',5);
        });
    }
}
