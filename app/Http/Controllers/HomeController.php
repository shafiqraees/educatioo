<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\ClassRoom;
use App\Models\LaunchQuiz;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\QuestionPaper;
use App\Models\QuestonPapersQuestion;
use App\Models\Teacher;
use App\Models\User;
use App\Models\UserQuiz;
use App\Models\UserQuizAttempt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\JsonResponse;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        /*$id =  $user = Auth::user()->id;

        $attempt_id = UserQuizAttempt::whereUserId($id)->whereQuestionPaperId(1)->pluck('question_option_id');
        $attempt = QuestionPaper::whereId(1)->whereHas('question')->withCount('question')->first();
        $option_data = QuestionOption::where('answer','!=','')->whereIn('id',$attempt_id)->count();
        if ($option_data){
            $result =  $option_data/$attempt->question_count*100;
        }
        dd($option_data);*/
        try {
            $id = $user = Auth::user()->id;
            #-------------------- for Test----------------------------------#
            $last_24Hours_test = UserQuizAttempt::whereUserId($id)
                ->where('created_at', '>=', \Carbon\Carbon::now()->subDay())
                ->whereNull('deleted_at')
                ->count();
            $last_7_Days_test = UserQuizAttempt::whereUserId($id)
                ->where('created_at', '>=', \Carbon\Carbon::today()->subDays(7))
                ->whereNull('deleted_at')
                ->count();
            $life_Time_test = UserQuizAttempt::whereUserId($id)->whereNull('deleted_at')->count();

            return view('user.dashboard',
                compact(
                    'last_24Hours_test',
                    'last_7_Days_test',
                    'life_Time_test'
                ));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editProfile() {
        try {
            $user = Auth::user();
            return view('user.home.profile', compact('user'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request) {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        try {
            $id = Auth::user()->id;
            $user = User::find($id);
            if ($request->hasFile('profile_pic')) {
                UpdateImageAllSizes($request, 'profile/', $user->profile_photo_path);
                $path = 'profile/' . $request->profile_pic->hashName();
            }
            $data = [
                'name' => $request->name,
                'profile_photo_path' => !empty($path) ? $path : $user->profile_photo_path,
            ];
            $user->update($data);
            return Redirect::back()->with('success', 'Profile updated successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function startQuiz() {
        try {
            //$currenttime = date("H:i");
            $current_user = Auth::user();
            $mytime = date('Y-m-d H:i:s');
            $data = "";
            $UserQuiz = "";
            //            dd($mytime);
            $launchqiz = LaunchQuiz::whereHas('userQuiz', function ($query) use ($current_user) {
                $query->whereUserId($current_user->id);
            })/*->whereDate('start_datetime', $mytime )*/ ->orderBY('id', 'DESC')->first();
            //            dd($launchqiz);
            if ($launchqiz) {
                if($launchqiz->status == 'Archive'){
                    return view('user.home.attempt', compact('data', 'UserQuiz'))->withErrors('Course ended - click to close.');
                }
                $attempt = UserQuizAttempt::whereUserId($current_user->id)->whereLaunchQuizId($launchqiz->id)->get();

                //                dd($attempt);
                if (!$attempt->isEmpty()) {
                    return view('user.home.attempt')->withErrors('You have already completed this course – click to close.');
                }
                //                if($mytime<$launchqiz->start_datetime)
                //                    dd(123, $mytime, $launchqiz->start_datetime);
                //                if($mytime<$launchqiz->end_datetime)
                //                    dd(456);
                //                dd(999);

                $startTime = date("H:i", strtotime($launchqiz->start_datetime));
                $endTime = date("H:i", strtotime($launchqiz->end_datetime));
                //if(($startTime == $currenttime) || (($endTime > $currenttime))){
                $UserQuiz = UserQuiz::whereLaunchQuizId($launchqiz->id)
                    ->whereTeacherId($launchqiz->teacher_id)
                    ->whereUserId($current_user->id)
                    ->first();
                $data = Question::whereQuestionPaperId($launchqiz->question_paper_id)->whereTeacherId($launchqiz->teacher_id)->first();
                //dd($data);
                return view('user.home.attempt', compact('data', 'UserQuiz'));
                /*} else {
                    return Redirect::back()->withErrors('please wait quiz not start yet.');
                }*/
            } else {
                //return Redirect::back()->withErrors('No quiz available');
                return view('user.home.attempt', compact('data', 'UserQuiz'))->withErrors('No session available.');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function attemptQuiz(Request $request) {
        //        dd($request->all());
        $validated = $request->validate([
            'question_option_id' => 'required',
        ]);
        try {
            $user = Auth::user();
            $check_record = UserQuizAttempt::/*whereQuestionPaperId($request->question_paper_id)->*/ whereUserId($user->id)
                ->whereLaunchQuizId($request->launch_quiz_id)/*->whereStatus('Completed')*/ ->first();
            //            dd($request->all(), $check_record);
//            if ($check_record) {
//                $data = [
//                    'question_paper_id' => !empty($request->question_paper_id) ? $request->question_paper_id : $check_record->question_paper_id,
//                    'class_room_id' => $user->class_room_id,
//                    'user_id' => $user->id,
//                    'question_id' => !empty($request->question_id) ? $request->question_id : $check_record->question_id,
//                    'question_option_id' => !empty($request->question_option_id) ? $request->question_option_id : $check_record->question_option_id,
//                    'launch_quiz_id' => !empty($request->launch_quiz_id) ? $request->launch_quiz_id : $check_record->launch_quiz_id,
//                    'user_quiz_id' => !empty($request->user_quiz_id) ? $request->user_quiz_id : $check_record->user_quiz_id,
//                ];
//                $check_record->update($data);
//            } else {
            $data = [
                'question_paper_id' => !empty($request->question_paper_id) ? $request->question_paper_id : "",
                'class_room_id' => $user->class_room_id,
                'user_id' => $user->id,
                'question_id' => !empty($request->question_id) ? $request->question_id : "",
                'question_option_id' => !empty($request->question_option_id) ? $request->question_option_id : "",
                'launch_quiz_id' => !empty($request->launch_quiz_id) ? $request->launch_quiz_id : "",
                'user_quiz_id' => !empty($request->user_quiz_id) ? $request->user_quiz_id : "",
                'status' => !empty($request->status) ? $request->status : "Pending",
            ];
            UserQuizAttempt::create($data);
//            }
            //$this->startQuizes($request->question_paper_id,$request->question_option_id,$request->question_id);
            return redirect(route('start.quizes', [
                'question_paper_id' => $request->question_paper_id,
                'question_option_id' => $request->question_option_id,
                'question_id' => $request->question_id,
                'launch_quiz_id' => $request->launch_quiz_id
            ]));
            //return Redirect::back()->with('success','Profile updated successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function startQuizes(Request $request) {
//        try {
        $result = 0;
        $current_user = Auth::user();
        $attempt = UserQuizAttempt::whereUserId($current_user->id)->whereLaunchQuizId($request->launch_quiz_id)->pluck('question_id');
        $UserQuiz = UserQuiz::whereUserId($current_user->id)
            ->whereLaunchQuizId($request->launch_quiz_id)
            ->whereHas('questionPaper')
            ->first();
//            dd($UserQuiz);
        if ($UserQuiz) {
            $question_data = Question::whereId($request->question_id)->first();
            $option_data = QuestionOption::whereId($request->question_option_id)->first();
            $attempted_question_ids = Question::whereIncludeInResult('true')->whereIn('id', $attempt)->pluck('id');
            $answer_data = QuestionOption::whereQuestionId($request->question_id)->where('answer', '!=', '')->first();
//                dd($question_data);
            if ($question_data->final_question == "true") {
                $include_in_result_ids = Question::whereIncludeInResult('true')->whereIn('id', $attempt)->pluck('id');
                $attempt_id = UserQuizAttempt::whereUserId($current_user->id)
                    ->whereLaunchQuizId($request->launch_quiz_id)
                    ->whereIn('question_id', $include_in_result_ids)
                    ->pluck('question_option_id');
                $total_question = Question::whereQuestionPaperId($request->question_paper_id)->whereIncludeInResult('true')->count();
                $total_question_points = Question::whereQuestionPaperId($request->question_paper_id)->whereIncludeInResult('true')->sum('marks');
                $attempted_option = QuestionOption::where('answer', '!=', '')->whereIn('id', $attempt_id)->count();
                $attempted_points = QuestionOption::with('question')->where('answer', '!=', '')->whereIn('id', $attempt_id)->get();
                $attempted_option_points = 0;
                foreach ($attempted_points as $attempted_point) {
                    $attempted_option_points += (float)$attempted_point->question->marks;
                }

                //dd($request->question_paper_id);
                if ($attempted_option) {
                    $result = $attempted_option / $total_question * 100;
                }
                //                    dd($total_question);
                $data = "";
                //return redirect(route('home'))->with('success','You have got '.$result.' % marks .' );
                return view('user.home.attempt',
                    compact('data',
                        'UserQuiz',
                        'question_data',
                        'option_data',
                        'answer_data',
                        'result',
                        'attempt',
                        'attempted_option',
                        'total_question_points',
                        'attempted_option_points'
                    )
                );
            }
            if ($attempted_question_ids) {
                $data = Question::whereQuestionPaperId($request->question_paper_id)
                    ->whereNotIn('id', $attempted_question_ids)->whereSerialId($option_data->suggested_question_id)->first();
            } else {
                $data = Question::whereQuestionPaperId($request->question_paper_id)
                    ->whereSerialId($option_data->suggested_question_id)->first();
            }
            //dd($attempted_question_ids);
            if ($data) {
                return view('user.home.attempt', compact('data', 'UserQuiz', 'question_data', 'option_data', 'answer_data'));
            } else {
                $include_in_result_ids = Question::whereIncludeInResult('true')->whereIn('id', $attempt)->pluck('id');
                $attempt_id = UserQuizAttempt::whereUserId($current_user->id)
                    ->whereLaunchQuizId($request->launch_quiz_id)
                    ->whereIn('question_id', $include_in_result_ids)
                    ->pluck('question_option_id');
                $total_question = Question::whereQuestionPaperId($request->question_paper_id)->whereIncludeInResult('true')->count();
                $attempted_option = QuestionOption::where('answer', '!=', '')->whereIn('id', $attempt_id)->count();

                //dd($request->question_paper_id);
                if ($attempted_option) {
                    $result = $attempted_option / $total_question * 100;
                }
                //                    dd($attempted_option);
                //return redirect(route('home'))->with('success','You have got '.$result.' % marks .' );
                return view('user.home.attempt',
                    compact('data',
                        'UserQuiz',
                        'question_data',
                        'option_data',
                        'answer_data',
                        'result',
                        'attempt',
                        'attempted_option')
                );
            }
        } else {
            return Redirect::back()->withErrors('paper completed');
        }
//        } catch (\Exception $e) {
//            DB::rollBack();
//            return Redirect::back()->withErrors('Sorry something went wrong');
//        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function quizAnswer(Request $request) {
        try {
            $user = User::find($request->id);
            if ($user) {
                $credentials = [
                    'email' => $user->email,
                    'password' => $user->org_password,
                    'is_active' => 'true'
                ];
                if (auth()->attempt($credentials)) {
                    return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $user);

                } else {
                    return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Your Pin is incorrect');
                }
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Trainee not found');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', 'Something went wrong');
        }
    }

    public function logOutUser() {
        try {
            Session::flush();
            Auth::logout();
            return $this->apiResponse(JsonResponse::HTTP_OK, 'data', 'logout');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', 'Something went wrong');
        }
    }
}
