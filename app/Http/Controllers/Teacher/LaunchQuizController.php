<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\LaunchQuiz;
use App\Models\MethodSetting;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\QuestionPaper;
use App\Models\User;
use App\Models\UserQuiz;
use App\Models\UserQuizAttempt;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App\Mail\SendQuizNotification;

class LaunchQuizController extends Controller {
    public function __construct() {
        $this->middleware('auth:teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $id = Auth::guard('teacher')->user()->id;

            $data = LaunchQuiz::whereTeacherId($id)->orderBy('id', 'desc')->paginate(10);

            return view('teacher.launchPaper.list', compact('data'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        try {
            $id = Auth::guard('teacher')->user()->id;
            $data = ClassRoom::whereStatus('Publish')->whereTeacherId($id)->whereNull('deleted_at')->orderBy('id', 'desc')->get();
            if ($data->count() <= 0)
                return redirect(route('classrooms.create'))->withErrors('Add Trainee Group First');
            $papers = QuestionPaper::whereTeacherId($id)->whereStatus('Publish')->whereNull('deleted_at')->orderBy('id', 'desc')->get();
            if ($papers->count() <= 0)
                return redirect(route('quiz.create'))->withErrors('Add Course First');
            return view('teacher.launchPaper.create', compact('data', 'papers'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validated = $request->validate([
            'paper_id' => 'required',
            'class_room' => 'required',
            'start_datetime' => 'required',
            'end_datetime' => 'required|after_or_equal:start_datetime',
        ]);
        $question_id = Question::whereQuestionPaperId($request->paper_id)->whereHas('option')->pluck('id');
        $final_question = Question::whereQuestionPaperId($request->paper_id)->whereFinalQuestion(true)->get();
        if ($final_question->isEmpty()) {
            return Redirect::back()->withErrors('Please add final question first.');
        }
        $serial_id = Question::whereQuestionPaperId($request->paper_id)->whereHas('option')->pluck('serial_id')->toArray();
        $exist = QuestionOption::whereIn('question_id', $question_id)->groupBy('suggested_question_id')->pluck('suggested_question_id')->toArray();
        $diff = array_diff($exist, $serial_id);
        if ($diff) {
            return Redirect::back()->withErrors('please first complete question you have suggested.' . implode(", ", $diff));
        }
        try {
            $students = User::whereClassRoomId($request->class_room)->whereIsActive('true')
                ->whereNull('deleted_at')->select('id', 'email', 'name', 'pincode')->get();

            $class_room = QuestionPaper::find($request->paper_id);
            $group = ClassRoom::find($request->class_room);
            DB::beginTransaction();
            if (!$students->isEmpty()) {
                $launch_quiz_check = LaunchQuiz::where('teacher_id', Auth::guard('teacher')->user()->id)
                    ->where('class_room_id', $request->class_room)
                    ->where('question_paper_id', $request->paper_id)
                    ->where('status', 'Unarchive')
                    ->where('end_datetime', '>', date('Y-m-d H:i:s', strtotime($request->end_datetime)))
                    ->get();
                if (!$launch_quiz_check->isEmpty()) {
                    return Redirect::back()
                        ->withErrors('You have already launched this quiz starting at ' . $launch_quiz_check[0]->start_datetime . ' and ending at ' . $launch_quiz_check[0]->end_datetime . '. Please wait for this time period to complete before launching again.');
                }
                $quiz_data = [
                    'class_room_id' => $request->class_room,
                    'question_paper_id' => $request->paper_id,
                    'start_datetime' => date('Y-m-d H:i:s', strtotime($request->start_datetime)),
                    'end_datetime' => date('Y-m-d H:i:s', strtotime($request->end_datetime)),
                    'teacher_id' => Auth::guard('teacher')->user()->id,
                ];
                // dd($quiz_data);
                $quiz = LaunchQuiz::create($quiz_data);
                if ($request->setting) {
                    foreach ($request->setting as $quiz_option) {
                        $option_data = [
                            'name' => $quiz_option,
                            'launch_quizze_id' => $quiz->id,
                        ];
                        MethodSetting::create($option_data);
                    }
                }
                foreach ($students as $student) {
                    $user_data = [
                        'teacher_id' => Auth::guard('teacher')->user()->id,
                        'question_paper_id' => $request->paper_id,
                        'class_room_id' => $request->class_room,
                        'user_id' => $student->id,
                        'launch_quiz_id' => $quiz->id,
                    ];
                    UserQuiz::create($user_data);

                    if (filter_var($student->email, FILTER_VALIDATE_EMAIL)) {
                        $details = [
                            'title' => 'Dear ' . $student->name,
                            'body' => 'Your course is about to start.  You can log in with the following information:',
                            'trainee_group' => $group->name,
                            'Course_Code' => $class_room->paper_code,
                            'pincode' => $student->pincode,
                        ];
                        Mail::to($student->email)->send(new SendQuizNotification($details));
                    }
                }
                DB::commit();
                return redirect(route('result.quiz'))->with('success', 'Quiz launch successfully.');
            } else {
                DB::rollBack();
                return Redirect::back()->withErrors('Please first add Students selected class.');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $ques = LaunchQuiz::find($id);
            if ($ques) {
                $data = $ques->delete();
                return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $data);
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Question not found');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function QuizPutToArchive($id) {
        try {
            $ques = LaunchQuiz::find($id);
            if ($ques) {
                $archive = [
                    'status' => 'Archive',
                ];
                $data = $ques->update($archive);
                return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $data);
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Question not found');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function archiveRevrt($id) {
        try {
            $ques = LaunchQuiz::find($id);
            if ($ques) {
                $archive = [
                    'status' => 'Unarchive',
                ];
                $data = $ques->update($archive);
                return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $data);
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Question not found');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function attemptQuiz($id) {
        try {
            $data = User::whereHas('userAttemptQuiz', function ($query) use ($id) {
                $query->where('launch_quiz_id', $id);
            })->with('userAttemptQuiz', function ($query) {
                $query->with('quizPaper', function ($query) {
                    $query->withCount('questionPaperquestion');
                });
            })->get();
            // dd($data);

            return view('teacher.launchPaper.attempt', compact('data'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function trineeResult($id, $classroom) {
        try {
            $data = User::with(['userAttemptQuiz' => function ($query) use ($id) {
                $query->where('launch_quiz_id', $id);
                $query->whereHas('question' , function ($subquery) {
                    $subquery->where('include_in_result', 'true');
                })->with('question');
                $query->with('launchQuiz.questionPaper.question');
            }])
                ->whereClassRoomId($classroom)
                ->whereIsActive('true')
                ->whereNull('deleted_at')
                ->where('teacher_id', Auth::guard('teacher')->user()->id)->get();
            //        dd($data->toArray());
            return view('teacher.launchPaper.trainee_result', compact('data', 'id', 'classroom'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    public
    function trineeResultTable($id, $classroom) {
        try {
//            $data = User::whereHas('userAttemptQuiz', function ($query) use ($id) {
//                $query->where('launch_quiz_id', $id);
//            })->with('userAttemptQuiz', function ($query) {
//                $query->whereHas('question', function ($subquery) {
//                    $subquery->where('include_in_result', 'true');
//                });
//                $query->with('question');
//            })->whereHas('userQuiz', function ($query2) use ($id) {
//                $query2->where('launch_quiz_id', $id);
//            })->with('userQuiz.questionPaper', function ($query2) {
//                $query2->with('question', function ($q) {
//                    $q->where('include_in_result', 'true');
//                });
//            })->get();
            $data = User::with(['userAttemptQuiz' => function ($query) use ($id) {
                $query->where('launch_quiz_id', $id);
                $query->whereHas('question' , function ($subquery) {
                    $subquery->where('include_in_result', 'true');
                })->with('question');
                $query->with('launchQuiz.questionPaper.question');
            }])
                ->whereClassRoomId($classroom)
                ->whereIsActive('true')
                ->whereNull('deleted_at')
                ->where('teacher_id', Auth::guard('teacher')->user()->id)->get();
            //        dd($data);
            return view('teacher.launchPaper.trainee_result_table', compact('data', 'id', 'classroom'));
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    public function courseEnd(Request $request, $id) {
        try {
            $ques = LaunchQuiz::find($id);
            if ($ques) {
                $archive = [
                    'status' => 'Archive',
                ];
                $data = $ques->update($archive);
                return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $data);
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Question not found');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }
    }
}
