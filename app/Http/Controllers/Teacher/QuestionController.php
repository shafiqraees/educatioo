<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\QuestionPaper;
use App\Traits\Transformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class QuestionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        try {
            $currrent_id = Auth::guard('teacher')->user()->id;
            $data = Question::whereTeacherId($currrent_id)->whereNull('deleted_at')->orderBy('id', 'desc')->get();
            return view('teacher.questions.list', compact('data'));

        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //dd(\request('id'));
        try {
            $id = Auth::guard('teacher')->user()->id;
            $course = QuestionPaper::find(\request('id'));
            //dd($course);
            $quiz = Question::whereTeacherId($id)
                ->whereQuestionPaperId(\request('id'))
                ->whereFinalQuestion('true')
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->count();

            $number = Question::whereTeacherId($id)
                ->whereQuestionPaperId(\request('id'))
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->value('serial_id');

            $quiz_number = isset($number) ? $number + 1 : 1;

            $data = Question::whereTeacherId($id)->whereQuestionPaperId(\request('id'))->whereNull('deleted_at')->orderBy('id', 'desc')->get();

            return view('teacher.questions.create', compact('quiz', 'quiz_number', 'course', 'data'));

        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validated = $request->validate([
            'name' => 'required',
            'photo' => 'sometimes|max:1024',
            'image.*' => 'sometimes|max:1024'
        ]);
        try {
            if ($request->hasFile('photo')) {
                SavePhotoAllSizes($request, 'quiz/');
                $quiz_image = 'quiz/' . $request->photo->hashName();
            }
            DB::beginTransaction();
            $serial = 1;
            $que_series = Question::whereTeacherId(Auth::guard('teacher')->user()->id)
                ->whereQuestionPaperId($request->id)
                ->orderBy('id', 'desc')
                ->first();
            if ($que_series) {
                $que_series = $que_series->serial_id + $serial;
            } else {
                $que_series = $serial;
            }
            $quiz_data = [
                'name' => $request->name,
                'teacher_id' => Auth::guard('teacher')->user()->id,
                'question_paper_id' => $request->id,
                'final_question' => isset($request->final_question) ? $request->final_question : "",
                'include_in_result' => ($request->include_in_result) ? $request->include_in_result : false,
                'marks' => $request->points,
                'serial_id' => isset($que_series) ? $que_series : "",
                'image' => !empty($quiz_image) ? $quiz_image : "default.png",
                'questio_code' => Hash::make($request->name . time()),
            ];
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                $path = [];
                foreach ($files as $file) {
                    SaveBannerAllSizes($file, 'quiz_options/');
                    $path[] .= 'quiz_options/' . $file->hashName();
                }
            }
            $quiz = Question::create($quiz_data);
            //dd($quiz);
            if ($request->question_id) {
                $question_ids = $request->question_id;

                foreach ($request->option as $key => $quiz_option) {
                    $answer = "";
                    if ($request->answer == $key) {
                        $answer = $request->answer;
                    }
                    $option_data = [
                        'question_id' => $quiz->id,
                        'suggested_question_id' => $question_ids[$key],
                        'answer' => isset($request->answer) && $request->answer == $key ? $request->answer : "",
                        'name' => $quiz_option,
                        'image' => !empty($path[$key]) ? $path[$key] : "default.png",
                    ];
                    QuestionOption::create($option_data);
                }
            }
            $id = $request->id;
            DB::commit();
            return redirect(route('question.create', ['id' => $request->id]))->with('success', 'Question created successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        try {
            $data = Question::whereId($id)->whereHas('option')->first();
            if ($data) {
                $allQuestions = Question::whereTeacherId($data->teacher_id)
                    ->whereQuestionPaperId($data->question_paper_id)
                    ->whereNull('deleted_at')
                    ->orderBy('id', 'desc')
                    ->get();
                return view('teacher.questions.edit', compact('data', 'allQuestions'));
            } else {
                return redirect(route('home'))->withErrors('Sorry record not found.');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //dd($request);
        $validated = $request->validate([
            'name' => 'required',
            'photo' => 'sometimes|max:1024',
            'image.*' => 'sometimes|max:1024'
        ]);

        try {
            if ($request->final_question && $request->final_question == "true") {
                $quiz = Question::whereTeacherId(Auth::guard('teacher')->user()->id)
                    ->whereQuestionPaperId(\request('id'))
                    ->whereFinalQuestion('true')
                    ->whereNull('deleted_at')
                    ->first();
                if ($quiz && $quiz->id != $id) {
                    return Redirect::back()->withErrors(['Sorry Final question already added.']);
                }
            }
            $data = Question::find($id);
            if ($request->hasFile('photo')) {
                UpdatePhotoAllSizes($request, 'quiz/', $data->image);
                $quz_path = 'quiz/' . $request->photo->hashName();
            }
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                $path = [];
                foreach ($files as $file) {
                    SaveBannerAllSizes($file, 'quiz_options/');
                    $path[] .= 'quiz_options/' . $file->hashName();
                }
            }
            if ($data) {
                DB::beginTransaction();
                $cat_data = [
                    'name' => !empty($request->name) ? $request->name : $data->name,
                    'teacher_id' => Auth::guard('teacher')->user()->id,
                    'image' => isset($quz_path) ? $quz_path : $data->image,
                    'marks' => !empty($request->points) ? $request->points : $data->marks,
                    'final_question' => isset($request->final_question) ? $request->final_question : "",
                    'include_in_result' => isset($request->include_in_result) ? $request->include_in_result : false,
                ];
                $data->update($cat_data);

                if (!empty($request->option)) {
                    $question_ids = $request->question_id;
                    $options_ids = $request->option_id;
                    $exist_option = QuestionOption::whereQuestionId($id)->pluck('id');
                    $i = 0;

                    foreach ($request->option as $key => $quiz_option) {
                        $answer = "";
                        if ($request->answer == $key) {
                            $answer = $request->answer;
                        }
                        if (!empty($options_ids[$key])) {

                            $check_edu = QuestionOption::whereId($options_ids[$key])->first();
                            if ($check_edu) {
                                $data_options = [
                                    'question_id' => $id,
                                    'suggested_question_id' => $question_ids[$key],
                                    'answer' => isset($request->answer) && $request->answer == $key ? $request->answer : "",
                                    'name' => $quiz_option,
                                    'image' => !empty($path[$key]) ? $path[$key] : $check_edu->image,
                                ];
                                QuestionOption::whereId($options_ids[$key])->update($data_options);
                                if ($options_ids[$key] == $exist_option[$i]) {
                                    unset($exist_option[$i]);
                                    $i++;
                                }
                            }
                        } else {
                            $doc_option = [];
                            $doc_option['question_id'] = $id;
                            $doc_option['suggested_question_id'] = $question_ids[$key] ? $question_ids[$key] : "";
                            $doc_option['answer'] = !empty($answer) ? $answer : "";
                            $doc_option['name'] = $quiz_option;
                            $doc_option['image'] = !empty($path[$key]) ? $path[$key] : 'default';
                            QuestionOption::Create($doc_option);
                        }
                    }

                    if (sizeof($exist_option) > 0) {
                        QuestionOption::whereId($exist_option[$i])->delete();
                    }
                }
                /*if (!empty($request->option)) {
                    $question_ids = $request->question_id;
                    $options_ids = $request->option_id;
                    $exist_option = QuestionOption::whereQuestionId($id)->pluck('id');
                    if ($exist_option) {
                        QuestionOption::whereQuestionId($id)->delete();
                        foreach ($request->option as  $key => $quiz_option) {
                            $answer = "";
                            if ($request->answer == $key) {
                                $answer = $request->answer;
                            }

                            $option_data = [
                                'question_id' => $id,
                                'suggested_question_id' => $question_ids[$key],
                                'answer' => $answer,
                                'name' => $quiz_option,
                                'image' => !empty($path[$key]) ? $path[$key] : "default.png",
                            ];
                            QuestionOption::create($option_data);
                        }
                    }
                }*/
                DB::commit();
                //return redirect(route('question.index'))->with('success', 'Question updated successfully.');
                return redirect(route('question.create', ['id' => $request->id]))->with('success', 'Question updated successfully.');
                //return Redirect::back()->with('success', 'Question updated successfully.');

            } else {
                return Redirect::back()->withErrors(['Sorry Record not found.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $ques = Question::find($id);
            if ($ques) {
                $option = QuestionOption::whereQuestionId($id)->delete();
                $data = $ques->delete();
                return $this->apiResponse(JsonResponse::HTTP_OK, 'data', $data);
            } else {
                return $this->apiResponse(JsonResponse::HTTP_NOT_FOUND, 'message', 'Question not found');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }
    }
}
